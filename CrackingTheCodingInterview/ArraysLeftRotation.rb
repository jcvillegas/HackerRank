####################################################################
#URL: https://www.hackerrank.com/challenges/ctci-array-left-rotation
####################################################################
number,rotation = gets.strip.split(' ')
number= number.to_i
rotation= rotation.to_i
array= gets.strip
array =array.split(' ').map(&:to_i)
(0..rotation-1).each { |i|array.push(array.slice!(0))}
array.each {|x| print "#{x} "}



