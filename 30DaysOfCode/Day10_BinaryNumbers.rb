#####################################################################
#URL: https://www.hackerrank.com/challenges/30-binary-numbers
#####################################################################
max=sum=0
gets.to_i.to_s(2).split('').each do |x| 
	if x=='0'
 		sum=0
 		next
 	else
 		sum+=1
 		max = max > sum ? max : sum	
	end
end

puts max

 