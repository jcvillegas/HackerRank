#####################################################################
#URL: https://www.hackerrank.com/challenges/30-2d-arrays
#####################################################################
arr = Array.new(6)
for arr_i in (0..6-1)
    arr_t = gets.strip
    arr[arr_i] = arr_t.split(' ').map(&:to_i)
end
max=-64
for x in (0..3)
    for y in (0..3)
        sum=0
        sum=arr[x][y]+arr[x][y+1]+arr[x][y+2]+arr[x+1][y+1]+arr[x+2][y]+arr[x+2][y+1]+arr[x+2][y+2]
        max = max > sum ? max : sum
    end
end
puts max