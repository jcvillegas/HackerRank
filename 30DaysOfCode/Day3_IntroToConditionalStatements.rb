#####################################################################
#URL: https://www.hackerrank.com/challenges/30-conditional-statements
#####################################################################

n=gets.to_i

if n.odd?
	puts 'Weird'
end

if n.even? and n.between?(2,5)
	puts 'Not Weird'
end

if n.even? and n.between?(6,20)
	puts 'Weird'
end

if n.even? and n > 20
	puts 'Not Weird'
end