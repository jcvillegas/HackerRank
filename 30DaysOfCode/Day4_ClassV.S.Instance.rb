#####################################################################
#URL: https://www.hackerrank.com/challenges/30-class-vs-instance
#####################################################################

class Person

	attr_accessor :age

	def initialize(initialAge)
		if initialAge>=0
			@age=initialAge
		else
			@age=0
			puts 'Age is not valid, setting age to 0.'
		end
	end

	def yearPasses()
		@age+=1
	end

	def amIOld()    
        if @age >= 18
            print("You are old.\n")
        elsif @age >= 13
            print("You are a teenager.\n")
        else
            print("You are young.\n")
        end
    end
end


   
    
  	
