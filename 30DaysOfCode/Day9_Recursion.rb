#####################################################################
#URL: https://www.hackerrank.com/challenges/30-recursion
#####################################################################

def factorial (n)
	 return (n == 1) ? 1 : n * factorial(n - 1);
end

puts factorial(gets.to_i)
