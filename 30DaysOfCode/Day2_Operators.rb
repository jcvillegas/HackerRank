####################################################################
#URL: https://www.hackerrank.com/challenges/30-operators
####################################################################

mealCost=gets.to_f
tipPercent=gets.to_f/100
taxPercent=gets.to_f/100
tip=mealCost*tipPercent
tax=mealCost*taxPercent
totalCost=(mealCost+tip+tax).round
puts "The total meal cost is #{totalCost} dollars."



