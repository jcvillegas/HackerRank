#####################################################################
#URL: https://www.hackerrank.com/challenges/30-dictionaries-and-maps
#####################################################################

phoneBook=Hash.new
nEntries=gets.to_i

nEntries.times do
	entrie=gets.split(' ')
	phoneBook[entrie[0]]=entrie[1]	
end

nEntries.times do
	name=gets.chomp    
    if phoneBook[name]==nil
    	puts 'Not found' 
    else
    	puts "#{name}=#{phoneBook[name]}"    	
    end    
end

