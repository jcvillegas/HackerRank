#####################################################################
#URL: https://www.hackerrank.com/challenges/30-inheritance
#####################################################################
class Person	
	attr_reader :firstName
	def initialize(firstName, lastName, id)
		@firstName = firstName
		@lastName = lastName
		@id = id
	end
	def printPerson()
		print("Name: ",@lastName , ", " + @firstName ,"\nID: " , @id)
	end
end


class Student <Person	

	def initialize(firstName, lastName, id, scores)
    	super(firstName, lastName, id)
    	@scores = scores
 	end

 	def calculate()
    	sum=0
    	for i in (0..@scores.length)
   			sum+=@scores[i].to_i
    	end
    	average=sum/@scores.length.to_i
    	if(average >= 90)
            return 'O' 
        elsif(average >= 80)
            return 'E' 
        elsif(average >= 70)
            return 'A' 
        elsif(average >= 55)
            return 'P' 
        elsif(average >= 40)
            return 'D' 
        else
            return 'T' 
        end
 	end
end

myStudent=Student.new('juan','villegas',5,[100,80])
puts myStudent.calculate

