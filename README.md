Practicing HackerRank challenges with Ruby language
===================================================

:metal: 30 Days of Code Challenges
---------------------------------------------------
	+ Day 0:  Hello, World
	+ Day 1:  Data Types
	+ Day 2:  Operators
	+ Day 3:  Intro to Conditional Statements
	+ Day 4:  Class vs. Instance
	+ Day 5:  Loops
	+ Day 6:  Lets Review
	+ Day 7:  Arrays
	+ Day 8:  DictionariesAndMaps
	+ Day 9:  Recursion
	+ Day 10: Binary Numbers
	+ Day 11: 2D Arrays
	+ Day 12: Inheritance
	+ Day 15: Linked List
	+ Day 16: Exceptions
	+ Day 17: More Exceptions

:metal: Cracking the Coding Interview
---------------------------------------------------
   + Arrays: Left Rotation

